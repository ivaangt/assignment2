
const FIRST_NAME = "Ioana";
const LAST_NAME = "Nagit";
const GRUPA = "1092";

/**
 * Make the implementation here
 */
function initCaching() {
    var cache = {};

    cache.getCache = function(){
        return cache;
    };

    cache.pageAccessCounter = function(counter= "home"){
        counter = counter.toLowerCase();
        if(cache[counter])
            cache[counter]++;
        else
            cache[counter] = 1;
    };

   
   return cache;
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

